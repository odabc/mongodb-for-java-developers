What will the following query do?

db.scores.find( { score : { $gt : 50 }, score : { $lt : 60 } } );

    -   Find all documents with score between 50 and 60
    -   Find all documents with score greater than 50
    +   Find all documents with score less than 60
    -   Explode like the Death Star
    -   None of the above

As it happens, the code that's on the screen will find all documents with score
less than 60. This is something of a trick question because what's going on
here is somewhat subtle. In fact, when JavaScript parses the input that's on the
screen inside the box it will construct a JavaScript object initially having a
score with the corresponding value the embedded document $ gt 50. And then the
second occurrence of score in that line will replace the first one. The reason
that we ask this question is because it's tempting to code that sort of thing
in at the JavaScript prompt. In fact, that's not going to work. In order to
achieve the desired effect, you either have to merge the $ gt and $ lt into a
sub-document embedded with the value of score or use the $ and operator to
achieve the effect of the logical intersection of those two criteria. So again,
the point is, although you can key that in the JavaScript prompt, the parse is
not necessarily what you might naively expect. In other languages that have a
similar kind of literal syntax for hash tables or dictionaries or whatever they
call them, you might find a similar kind of effect. The last one might win or
the first one might win or it might be a syntax error.