package course.homework;

import com.mongodb.MongoClient;
import com.mongodb.client.*;
import org.bson.Document;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;

public class MongoDBRequest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();

        MongoDatabase database = client.getDatabase("students");
        final MongoCollection<Document> collection = database.getCollection("grades");

        // db.grades.find({'type':"homework"}).sort({'student_id':1, 'score':1})
        FindIterable<Document> results = collection.find(eq("type", "homework"))
                .sort(orderBy(ascending("student_id"), ascending("score")));

        Object id = null;
        for (Document cur : results) {
            if (id == null || !id.equals(cur.get("student_id"))) {
                id = cur.get("student_id");
                collection.deleteOne(eq("_id", cur.getObjectId("_id")));
            }
        }

    }
}
