package course.homework;

import com.mongodb.MongoClient;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.Arrays;
import java.util.List;

public class MongoDBRequest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();

        MongoDatabase database = client.getDatabase("school");
        final MongoCollection<Document> collection = database.getCollection("students");

        // db.students.aggregate({$unwind: "$scores"},
        //                       {$match: {"scores.type":"homework"}},
        //                       {$group: {_id:"$_id", minimum:{$min:"$scores.score"}}})
        List<Document> request = Arrays.asList(
                new Document("$unwind", "$scores"),
                new Document("$match", new Document("scores.type", "homework")),
                new Document("$group", new Document("_id", "$_id")
                        .append("minimum", new Document("$min", "$scores.score")))
        );

        AggregateIterable<Document> results = collection.aggregate(request);

        // db.students.update({_id:"_id"},
        //                    {$pull:{scores:{score: "minimum"}}},
        for (Document cur : results) {
            collection.updateOne(new Document("_id", cur.get("_id")),
                    new Document("$pull",
                            new Document("scores",
                                    new Document("score", cur.get("minimum")))));
            System.out.println(cur.toJson());
        }
    }
}

